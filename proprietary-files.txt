# All unpinned files are extracted from oriole SQ3A.220705.003

# GMS mandatory core packages
# GoogleCalendarSyncAdapter, not exisiting on redfin because it is overridden by CalendarGooglePrebuilt
-product/app/GoogleContactsSyncAdapter/GoogleContactsSyncAdapter.apk;PRESIGNED
-product/app/GoogleTTS/GoogleTTS.apk;OVERRIDES=PicoTts;PRESIGNED
-product/app/LocationHistoryPrebuilt/LocationHistoryPrebuilt.apk;PRESIGNED
-product/app/WebViewGoogle-Stub/WebViewGoogle-Stub.apk;OVERRIDES=webview;PRESIGNED
product/app/WebViewGoogle/WebViewGoogle.apk.gz
-product/priv-app/AndroidAutoStubPrebuilt/AndroidAutoStubPrebuilt.apk;PRESIGNED
-product/priv-app/ConfigUpdater/ConfigUpdater.apk;PRESIGNED
-product/priv-app/GoogleOneTimeInitializer/GoogleOneTimeInitializer.apk;OVERRIDES=OneTimeInitializer;PRESIGNED
-product/priv-app/GoogleRestorePrebuilt/GoogleRestorePrebuilt.apk;PRESIGNED
-product/priv-app/PartnerSetupPrebuilt/PartnerSetupPrebuilt.apk;PRESIGNED
-product/priv-app/Phonesky/Phonesky.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/m/independent/AndroidPlatformServices.apk;PRESIGNED
-product/priv-app/PrebuiltGmsCore/PrebuiltGmsCoreSc.apk;OVERRIDES=NetworkRecommendation;PRESIGNED
-product/priv-app/SetupWizardPrebuilt/SetupWizardPrebuilt.apk;OVERRIDES=Provision;PRESIGNED
-product/priv-app/WellbeingPrebuilt/WellbeingPrebuilt.apk;PRESIGNED
-system/app/GoogleExtShared/GoogleExtShared.apk;OVERRIDES=ExtShared;PRESIGNED
-system/app/GooglePrintRecommendationService/GooglePrintRecommendationService.apk;OVERRIDES=PrintRecommendationService;PRESIGNED
-system_ext/priv-app/GoogleFeedback/GoogleFeedback.apk;PRESIGNED
-system_ext/priv-app/GoogleServicesFramework/GoogleServicesFramework.apk;PRESIGNED

# GMS mandatory application packages
# Duo, not existing on redfin
product/app/Chrome/Chrome.apk.gz
-product/app/Chrome-Stub/Chrome-Stub.apk;OVERRIDES=Browser,Browser2,Jelly;PRESIGNED
-product/app/Drive/Drive.apk;PRESIGNED
-product/app/Maps/Maps.apk;PRESIGNED
-product/app/Photos/Photos.apk;OVERRIDES=Gallery2,SnapdragonGallery;PRESIGNED
-product/app/PrebuiltGmail/PrebuiltGmail.apk;OVERRIDES=Email,Exchange2;PRESIGNED
-product/app/TrichromeLibrary-Stub/TrichromeLibrary-Stub.apk;PRESIGNED
product/app/TrichromeLibrary/TrichromeLibrary.apk.gz
-product/app/Videos/Videos.apk;PRESIGNED
-product/app/YouTube/YouTube.apk;PRESIGNED
-product/app/GoogleSounds/GoogleSounds.apk;PRESIGNED
-product/priv-app/Velvet/Velvet.apk;OVERRIDES=QuickSearchBox;PRESIGNED

# GMS optional application packages
# Keep, not existing on redfin
-product/app/CalculatorGooglePrebuilt/CalculatorGooglePrebuilt.apk;OVERRIDES=Calculator,ExactCalculator;PRESIGNED
-product/app/CalendarGooglePrebuilt/CalendarGooglePrebuilt.apk;OVERRIDES=Calendar,GoogleCalendarSyncAdapter,Etar;PRESIGNED
-product/app/LatinIMEGooglePrebuilt/LatinIMEGooglePrebuilt.apk;OVERRIDES=LatinIME;PRESIGNED
-product/app/PrebuiltDeskClockGoogle/PrebuiltDeskClockGoogle.apk;OVERRIDES=AlarmClock,DeskClock;PRESIGNED
-product/app/talkback/talkback.apk;PRESIGNED
-product/priv-app/FilesPrebuilt/FilesPrebuilt.apk;PRESIGNED
-system/priv-app/TagGoogle/TagGoogle.apk;OVERRIDES=Tag;PRESIGNED

# GMS comms suite
# CarrierServices, existing on redfin but not used in favor of CarrierConfig
-product/app/GoogleContacts/GoogleContacts.apk;OVERRIDES=Contacts;PRESIGNED
product/etc/permissions/com.google.android.apps.dialer.call_recording_audio.features.xml|42358ffb4f5a3bbdb7175fa9a0b37d3a350f1468
product/etc/permissions/com.google.android.dialer.support.xml
-product/framework/com.google.android.dialer.support.jar;PRESIGNED
-product/priv-app/GoogleDialer/GoogleDialer.apk;OVERRIDES=Dialer;PRESIGNED
-product/priv-app/PrebuiltBugle/PrebuiltBugle.apk;OVERRIDES=messaging;PRESIGNED

# GMS turbo
-product/priv-app/TurboPrebuilt/TurboPrebuilt.apk;PRESIGNED

# Configuration files
product/etc/default-permissions/default-permissions.xml
product/etc/permissions/privapp-permissions-google-p.xml
product/etc/permissions/split-permissions-google.xml
product/etc/preferred-apps/google.xml
product/etc/security/fsverity/gms_fsverity_cert.der
product/etc/security/fsverity/play_store_fsi_cert.der
product/etc/sysconfig/google_build.xml
product/etc/sysconfig/google-hiddenapi-package-whitelist.xml
product/etc/sysconfig/google.xml
product/etc/sysconfig/nexus.xml
product/etc/sysconfig/pixel_experience_2017.xml
product/etc/sysconfig/pixel_experience_2018.xml
product/etc/sysconfig/pixel_experience_2019_midyear.xml
product/etc/sysconfig/pixel_experience_2019.xml
product/etc/sysconfig/pixel_experience_2020_midyear.xml
product/etc/sysconfig/pixel_experience_2020.xml
system/etc/permissions/privapp-permissions-google.xml
system_ext/etc/permissions/privapp-permissions-google-se.xml

# Pixel application packages
-product/app/MarkupGoogle/MarkupGoogle.apk;PRESIGNED
-product/priv-app/DeviceIntelligenceNetworkPrebuilt/DeviceIntelligenceNetworkPrebuilt.apk;PRESIGNED
-product/priv-app/DevicePersonalizationPrebuiltPixel2020/DevicePersonalizationPrebuiltPixel2020.apk;PRESIGNED
-system_ext/priv-app/PixelSetupWizard/PixelSetupWizard.apk;OVERRIDES=LineageSetupWizard;PRESIGNED
-system/priv-app/DocumentsUIGoogle/DocumentsUIGoogle.apk;OVERRIDES=DocumentsUI;PRESIGNED

# Pixel fonts
product/fonts/GoogleSans-Italic.ttf
product/fonts/GoogleSans-Regular.ttf

# Google extension services (extracted from com.google.android.extservices.apex) - from oriole SQ3A.220705.003
-system/priv-app/GoogleExtServices/GoogleExtServices.apk;OVERRIDES=ExtServices;PRESIGNED|271919f1d99139c06d3a95a9ffeb579f9ba33e54
